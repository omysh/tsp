package data.users;

public class AccountRepository {
	
	private static volatile AccountRepository instance = null;
	
	private AccountRepository() {
	}

	public static AccountRepository get() {
		if (instance == null) {
			synchronized (AccountRepository.class) {
				if (instance == null) {
					instance = new AccountRepository();
				}
			}
		}
		return instance;
	}
	
	public IAccount getCorectUser() {
		return Account.get()
				.setAccountLogin("testuser5624@gmail.com")
				.setAccountPassword("testuser5624qwerty")
				.build()
				.setAccountFirstName("testuser")
				.setAccountLastName("testuser");
	}
	
	public IAccount getUncorectLoginUser() {
		return Account.get()
				.setAccountLogin("testuser5634@gmail.com")
				.setAccountPassword("testuser5624qwerty")
				.build()
				.setAccountFirstName("testuser")
				.setAccountLastName("testuser");
	}
	
	public IAccount getUncorectPasswordUser() {
		return Account.get()
				.setAccountLogin("testuser5624@gmail.com")
				.setAccountPassword("testuser1624qwerty")
				.build()
				.setAccountFirstName("testuser")
				.setAccountLastName("testuser");
	}
	
	
}

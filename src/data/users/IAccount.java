package data.users;

public interface IAccount {
	
	String getAccountLogin();
	String getAccountPassword();
	String getAccountFirstName();
	String getAccountLastName();
	
}

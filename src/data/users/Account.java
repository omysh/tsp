package data.users;

interface IAccountLogin{
	IAccountPassword setAccountLogin(String accountLogin);
}

interface IAccountPassword{
	IBuildAccount setAccountPassword(String accountPassword);
}

interface IBuildAccount {
	IAccountExt build();
}
public class Account implements IAccountLogin, IAccountPassword, IBuildAccount, IAccountExt {

	private String accountLogin;
	private String accountPassword;
	private String accountFirstName;
	private String accountLastName;
	
	private Account() {
		this.accountFirstName = new String();
		this.accountLastName = new String();
	}
	
	// static factory - - - - - - - - - -
	
	public static IAccountLogin get() {
		return new Account();
	}
	
	// set - - - - - - - - - -
	
	public IAccountPassword setAccountLogin(String accountLogin) {
		this.accountLogin = accountLogin;
		return this;
	}
	
	public IBuildAccount setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
		return this;
	}

	public Account build() {
		return this;
	}
	
	public IAccountExt setAccountFirstName(String accountFirstName) {
		this.accountFirstName = accountFirstName;
		return this;
	}

	public IAccountExt setAccountLastName(String accountLastName) {
		this.accountLastName = accountLastName;
		return this;
	}
	
	// get - - - - - - - - - -

	public String getAccountLogin() {
		return accountLogin;
	}

	public String getAccountPassword() {
		return accountPassword;
	}

	public String getAccountFirstName() {
		return accountFirstName;
	}
	
	public String getAccountLastName() {
		return accountLastName;
	}
	
}

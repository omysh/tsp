package data.users;

public interface IAccountExt extends IAccount {
	
	IAccountExt setAccountFirstName(String accountFirstName);
	IAccountExt setAccountLastName(String accountLastName);
	
}

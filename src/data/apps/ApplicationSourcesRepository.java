package data.apps;

public class ApplicationSourcesRepository {
    private static volatile ApplicationSourcesRepository instance = null;

	private ApplicationSourcesRepository() {
	}
	
    public static ApplicationSourcesRepository get() {
        if (instance == null) {
            synchronized (ApplicationSourcesRepository.class) {
                if (instance == null) {
                    instance = new ApplicationSourcesRepository();
                }
            }
        }
        return instance;
    }

    public ApplicationSources getGoogleByFirefox() {
    	return ApplicationSources.get()
    			.setLoginUrl("https://www.google.com/")
    			.setLogoutUrl("https://accounts.google.com/Logout")
    			.setImplicitTimeOut(5)
    			.setBrowserName("firefox");
    }
}

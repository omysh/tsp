package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	protected WebDriver driver;
	private WebElement accountMenu;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		this.accountMenu = driver.findElement(By.cssSelector("span.gb_3a.gbii"));
	}

	public WebElement getAccountMenu() {
		return this.accountMenu;
	}

	public String getAtributeTitle() {
		return getAccountMenu().getAttribute("title");
	}
	
	public void clickAccountMenu() {
		getAccountMenu().click();
	}

	public LogoutPage goToLogout() {
		clickAccountMenu();
		return new LogoutPage(driver);
	}

}

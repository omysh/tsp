package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogoutPage {
	
	protected WebDriver driver;
	private WebElement exitBtn;

	public LogoutPage(WebDriver driver) {
		this.driver = driver;
		this.exitBtn = driver.findElement(By.id("signout"));
	}

	public WebElement getExitBtn() {
		return this.exitBtn;
	}
	
	public void clickExitBtn() {
		getExitBtn().click();
	}

	public StartPage logout() {
		clickExitBtn();
		return new StartPage(driver);
	}
	
}

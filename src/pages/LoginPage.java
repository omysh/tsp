package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import data.users.IAccount;

public class LoginPage {
	
	private WebDriver driver;
	
	//--------------------------------------------------------------------------------------------	
	
	private class LoginPageUIMap {
		public final WebElement loginField;
		public final WebElement nextBtn;

		public LoginPageUIMap() {

			this.loginField = driver.findElement(By.id("Email"));
			this.nextBtn = driver.findElement(By.id("next"));
		}
	}

	// --------------------------------------------------------------------

	private LoginPageUIMap controlsLogin;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		controlsLogin = new LoginPageUIMap();
	}
	
	// PageObject - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	// Get Elements - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	public WebElement getLoginField() {
		return this.controlsLogin.loginField;
	}
	
	public WebElement getNextBtn() {
		return this.controlsLogin.nextBtn;
	}

	// Set Data - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	public void setLogin(String accountLogin) {
		getLoginField().sendKeys(accountLogin);
	}

	public void clearLogin() {
		getLoginField().clear();
	}

	public void clickLogin() {
		getLoginField().click();
	}
	
	public void clickNextBtn() {
		getNextBtn().click();
	}
	
	// business - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// Functional
	
	public PasswordPage goToPassPage(IAccount account) {
		clickLogin();
		clearLogin();
		setLogin(account.getAccountLogin());
		clickNextBtn();
		return new PasswordPage(driver);
	}
	
}

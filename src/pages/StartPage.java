package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StartPage {
	
	protected WebDriver driver;
	private WebElement signin;
	
	public StartPage(WebDriver driver) {
		this.driver = driver;
		this.signin = driver.findElement(By.id("gb_70"));
	}

	public WebElement getSignin() {
		return this.signin;
	}

	public void clickSignin() {
		getSignin().click();
	}
	
	// Business Logic
	// Functional
	
	   public LoginPage goToLogin() {
		   clickSignin();
	        return new LoginPage(driver);
	    }
	
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import data.users.IAccount;

public class PasswordPage {

	private WebDriver driver;

	// --------------------------------------------------------------------------------------------

	private class PasswordPageUIMap {
		public final WebElement passwordField;
		public final WebElement signInBtn;

		public PasswordPageUIMap() {
			this.passwordField = driver.findElement(By.name("Passwd"));
			this.signInBtn = driver.findElement(By.id("signIn"));
		}
	}

	// --------------------------------------------------------------------------------------------

	private PasswordPageUIMap controlsPassword;

	public PasswordPage(WebDriver driver) {
		this.driver = driver;
		this.controlsPassword = new PasswordPageUIMap();
	}

	// PageObject - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	// Get Elements - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public WebElement getPasswordField() {
		return this.controlsPassword.passwordField;
	}

	public WebElement getSignInBtn() {
		return this.controlsPassword.signInBtn;
	}

	// Set Data - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	public void setPassword(String accountPassword) {
		getPasswordField().sendKeys(accountPassword);
	}

	public void clearPassword() {
		getPasswordField().clear();
	}

	public void clickPassword() {
		getPasswordField().click();
	}

	public void clickSignInBtn() {
		getSignInBtn().click();
	}

	// business - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// Functional

	public HomePage goToHomePage(IAccount account) {
		clickPassword();
		clearPassword();
		setPassword(account.getAccountPassword());
		clickSignInBtn();
		return new HomePage(driver);
	}
	
	public ValidatorPage goToValidatorPage(IAccount account) {
		clickPassword();
		clearPassword();
		setPassword(account.getAccountPassword());
		clickSignInBtn();
		return new ValidatorPage(driver);
	}

}

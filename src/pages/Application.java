package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import data.apps.ApplicationSources;

public class Application {
    
    ApplicationSources applicationSources;
    WebDriver driver;
    
    public Application(ApplicationSources applicationSources) {
    	this.applicationSources= applicationSources;
    }

    private void initBrowser() {
    	if (driver == null){
	    	driver = new FirefoxDriver();
	    	driver.manage().timeouts()
	    		.implicitlyWait(applicationSources.getImplicitTimeOut(), TimeUnit.SECONDS);
    	}
    }
    
    public StartPage load() {
    	initBrowser();
    	driver.get(applicationSources.getLoginUrl());
        return new StartPage(driver);
    }

    public StartPage login() {
    	if (driver == null){
    		return load();
    	}
    	driver.get(applicationSources.getLogoutUrl());
        driver.get(applicationSources.getLoginUrl());
        return new StartPage(driver);
    }

    public StartPage logout() {
    	if (driver == null){
    		return load();
    	}
    	driver.get(applicationSources.getLogoutUrl());
        return new StartPage(driver);
    }
    
    public void quit() {
    	if (driver != null){
    		driver.quit();
    	}
    }
    
    public void quitAll() {
    	if (driver != null){
    		driver.quit();
    	}
    }
    
}

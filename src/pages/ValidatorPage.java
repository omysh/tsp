package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ValidatorPage {
	
	public static final String VALIDATOR_MESSAGE = "������ ������������ ����� ��� ������";
	
	protected WebDriver driver;
	private WebElement validator;

	public ValidatorPage(WebDriver driver) {
		this.driver = driver;
		this.validator = driver.findElement(By.id("errormsg_0_Passwd"));
	}

	public WebElement getValidator() {
		return this.validator;
	}
	
	public String getValidatorText() {
		return getValidator().getText();
	}
}

package tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import data.apps.ApplicationSourcesRepository;
import data.users.AccountRepository;
import data.users.IAccount;
import pages.Application;
import pages.HomePage;

public class SmokeLoginTests {

	private Application application;

	@BeforeClass
	public void oneTimeSetUp() {
		application = new Application(ApplicationSourcesRepository.get().getGoogleByFirefox());
	}

	@DataProvider
	public Object[][] Users() {
		return new Object[][] { { AccountRepository.get().getCorectUser() }, };
	}

	@Test(dataProvider = "Users")
	public void corectLogin(IAccount corectUser) {
		HomePage homePage = application.load().goToLogin().goToPassPage(corectUser).goToHomePage(corectUser);
		int lengthTitleEnd = homePage.getAtributeTitle().length();
		int lengthTitleStart = homePage.getAtributeTitle().indexOf(AccountRepository.get().getCorectUser().getAccountLogin());
		String email = homePage.getAtributeTitle().substring(lengthTitleStart, lengthTitleEnd-1);
		Assert.assertEquals(email, AccountRepository.get().getCorectUser().getAccountLogin());
	}

	@AfterClass
	public void oneTimeTearDown() {
		application.quitAll();
	}
	
}

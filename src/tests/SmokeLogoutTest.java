package tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import data.apps.ApplicationSourcesRepository;
import data.users.AccountRepository;
import pages.Application;
import pages.HomePage;
import pages.StartPage;

public class SmokeLogoutTest {
	private Application application;
	private HomePage homePage;
	
	@BeforeClass
	public void oneTimeSetUp() {
		application = new Application(ApplicationSourcesRepository.get().getGoogleByFirefox());
		homePage = application.load().goToLogin().goToPassPage(AccountRepository.get().getCorectUser()).goToHomePage(AccountRepository.get().getCorectUser());
	}

	@Test
	public void corectLogin() {
		StartPage startPage = homePage.goToLogout().logout();	
		Assert.assertTrue(startPage.getSignin().isDisplayed());
	}

	@AfterClass
	public void oneTimeTearDown() {
		application.quitAll();
	}
	
}

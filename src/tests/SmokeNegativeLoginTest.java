package tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import data.apps.ApplicationSourcesRepository;
import data.users.AccountRepository;
import data.users.IAccount;
import pages.Application;
import pages.ValidatorPage;

public class SmokeNegativeLoginTest {
	
	private Application application;

	@BeforeClass
	public void oneTimeSetUp() {
		application = new Application(ApplicationSourcesRepository.get().getGoogleByFirefox());
	}

	@DataProvider
	public Object[][] Users() {
		return new Object[][] { { AccountRepository.get().getUncorectPasswordUser() }, };
	}

	@Test(dataProvider = "Users")
	public void corectLogin(IAccount uncorectUser) {
		ValidatorPage validator = application.load().goToLogin().goToPassPage(uncorectUser).goToValidatorPage(uncorectUser);
		Assert.assertEquals(validator.getValidatorText(), ValidatorPage.VALIDATOR_MESSAGE);
	}

	@AfterClass
	public void oneTimeTearDown() {
		application.quitAll();
	}
	
}
